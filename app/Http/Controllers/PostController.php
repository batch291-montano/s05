<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// access the authenticated user via Auth Facade
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;


class PostController extends Controller
{
    
    public function create()
    {
        return view('posts.create');
    }


    public function store(Request $request)
    {
        if(Auth::user()) {
            // instantiate a new Post object from the Post Model
            $post = new Post;
            // define the properties of the $post object using the received form data.
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            // get the id of the authenticated user and set it as the foreign key for the user_id of the new post.
            $post->user_id = (Auth::user()->id);
            // save this $post object into the database.
            $post->save();

            return redirect('/posts');
        } else {
            return redirect('/login');
        }
    }

    // action that will return a view showing all the blog posts.
    public function index()
    {
        $posts = Post::where('isActive', true)->get();
        return view('posts.index')->with('posts', $posts);
    }

   

    // action for showing only the posts authored by the authenticated user.
    public function myPosts()
    {
        if(Auth::user()){
            $posts = Auth::user()->posts;

            return view('posts.index')->with('posts', $posts);
        } else {
            return redirect('/login');
        }
    }


    //Solution S02 - Activity
        //action that will return a view showing random blog posts
    public function welcome()
    {
        $posts = Post::where('isActive', true)->inRandomOrder()->take(3)->get();

        return view('welcome')->with('posts', $posts);
    }

    // action that will return a view showing a specific post using the the URL parameter $id to query for the database entry to be shown.
    public function show($id)
    {
        $post = Post::find($id);
        return view('posts.show')->with('post', $post);
    }


    // S03 - Activity Solution
    // action that will return a view showing an edit post form for a specific post using the URL parameter $id to query for the database entry to be edited.

    //  public function edit($id)
    // {
    //     $post = Post::find($id);
    //     return view('posts.edit')->with('post', $post);
    // }

    // S03 Activity Stretch Goal - Solution
    public function edit($id)
    {
        $post = Post::find($id);
        if(Auth::user()){
            if(Auth::user()->id == $post->user_id){
               
                return view('posts.edit')->with('post', $post);
            }
            return redirect('/posts');
        }
        else{
            return redirect('/login');
        }
    }


    public function update(Request $request, $id)
    {
        $post = Post::find($id);

        // if authenticated user's ID is the same as the post's user_id
        if(Auth::user()->id == $post->user_id) {
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->save();
        }

        return redirect('/posts');
    }


    public function destroy($id)
    {
        $post = Post::find($id);

        if(Auth::user()->id == $post->user_id) {
            $post->delete();
        }

        return redirect('/posts');
    }

    // S04 Activity Solution
    public function archive($id)
        {
            $post = Post::find($id);
            if(Auth::user()->id == $post->user_id) {
                $post->isActive = false;
                $post->save();
            }

            return redirect('/myPosts');
        }

    //S04 Activity solution Version 2
    public function unarchive($id)
        {
            $post = Post::find($id);
            if(Auth::user()->id == $post->user_id) {
                $post->isActive = true;
                $post->save();
            }

            return redirect('/posts');
        }


    public function like($id)
    {
        $post = Post::find($id);
        $user_id = Auth::user()->id;

        // if authenticated user is not the post author
        if($post->user_id != $user_id){
            // checks if a post like has been made by the logged in user before
            if($post->likes->contains("user_id", $user_id)) {
                // deletes the like made by the user to unlike the post.
                PostLike::where('post_id', $post->id)->where('user_id', $user_id)->delete();
            } else {
                // to instantiate a new PostLike object from the PostLike model.
                $postLike = new PostLike;
                // define the properties of the $postLike object
                $postLike->post_id = $post->id;
                $postLike->user_id = $user_id;

                // save the $postLike object into the database.
                $postLike->save();
            }
            // redirect user back to the original post.
            return redirect("/posts/$id");

        }

    }

    public function comment(Request $request, $id)
{
    $post = Post::find($id);
    $user_id = Auth::user()->id;

    if($post->user_id != $user_id){
       
        if($post->comments->contains("user_id", $user_id)) {
        
            PostComment::where('post_id', $post->id)->where('user_id', $user_id)->delete();
        } else {
            $comment = new PostComment;
            $comment->content = $request->input('content');
            $comment->post_id = $post->id;
            $comment->user_id = $user_id;
                
            $comment->save();
        }
        return redirect("/posts/$id")->with('success', 'Comment added successfully.');
    }
}


}    