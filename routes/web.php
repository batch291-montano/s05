<?php

use Illuminate\Support\Facades\Route;

// link the PostController file.
use App\Http\Controllers\PostController;
use App\Models\Post;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', [PostController::class, 'welcome']);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// define a route wherein a view to create a post will be returned to the user.
Route::get('/posts/create', [PostController::class, 'create']);

// define a route wherein form data will be sent via POST method to the /posts URI endpoint.
Route::post('/posts', [PostController::class, 'store']);

// define a route that will return a view containing all the posts.
Route::get('/posts', [PostController::class, 'index']);

// define a route that will return a view containing only the authenticated user's posts.
Route::get('/myPosts', [PostController::class, 'myPosts']);

// define a route wherein a view showing a specific post with the matching URL parameter ID will be returned to the user.
Route::get('/posts/{id}', [PostController::class, 'show']);

// define a route for viewing the edit post form - S03 Activity Solution
Route::get('/posts/{id}/edit', [PostController::class, 'edit']);

// define a route that will overwrite an existing post with matching URL parameter ID via PUT method
Route::put('/posts/{id}', [PostController::class, 'update']);

// S04 Activity Solution

//define a route that will delete a post of the matching URL parameter ID
// Route::delete('/posts/{id}', [PostController::class, 'archive']);

// S04 Activity Solution - Version 2
// define a route that will soft-delete a post of the matching URL parameter ID.
Route::get('/posts/{id}/archive', [PostController::class, 'archive']);

// define a route that will reactivate a post of the matching URL parameter ID.
Route::get('/posts/{id}/unarchive', [PostController::class, 'unarchive']);

// define a route that will allow users to like a posts.
Route::put('/posts/{id}/like', [PostController::class, 'like']);

Route::put('/posts/{id}/comment', [PostController::class, 'comment']);
