@extends('layouts.app')


@section('content')

	<div class="card">
		<div class="card-body">
			<h2 class="card-title">{{$post->title}}</h2>
			<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class="card-subtitle text-muted mb-3">Created at {{$post->created_at}}</p>
			<p class="card-text">{{$post->content}}</p>

			@if(Auth::id() != $post->user_id)
				<form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
					
					@method('PUT')
					@csrf

					@if($post->likes->contains("user_id", Auth::id()))
						<button type="submit" class="btn btn-danger">Unlike</button>
					@else
						<button type="submit" class="btn btn-success">Like</button>
					@endif
				</form>
			@endif

			<form class="d-inline" method="POST" action="/posts/{{$post->id}}/comment">
				@method('PUT')
				@csrf
				<!-- Button trigger modal -->
				<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#commentModal">
				Add Comment
				</button>

				<!-- Modal -->
				<div class="modal fade" id="commentModal" tabindex="-1" aria-labelledby="commentModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="commentModalLabel">Add Comment</h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
					<div class="modal-body">
						<form method="POST" action="/posts/{{$post->id}}">
						@method('PUT')
						@csrf
						<div class="form-group">
							<label for="content">Comment Content:</label>
							<textarea class="form-control" id="content" name="content" rows="3" required></textarea>
						</div>
						<button type="submit" class="btn btn-primary my-3">Add Comment</button>
						<button type="submit" class="btn btn-secondary my-3">Close</button>		
						</form>
					</div>
					</div>
				</div>
				</div>

				<div class="mt-4">
					<h2>Comments</h2>
					@if ($post->comments->isEmpty())
						<p>No comments yet. Be the first to comment!</p>
					@else
						<ul class="list-group mt-3">
						@foreach ($post->comments as $comment)
							<li class="list-group-item">
							<div class="fw-bold">{{ $comment->user->name }}</div>
							<div>{{ $comment->content }}</div>
							</li>
						@endforeach
						</ul>
					@endif
				</div>
			</form>
			<div class="mt-3">
				<a href="/posts" class="card-link">View all posts</a>	
			</div>
		</div>
	</div>
@endsection